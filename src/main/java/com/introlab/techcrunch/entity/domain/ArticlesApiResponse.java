package com.introlab.techcrunch.entity.domain;

import com.introlab.techcrunch.entity.dto.Article;
import lombok.Data;

import java.util.List;

@Data
public class ArticlesApiResponse {
    private String status;
    private String source;
    private String sortBy;
    private List<Article> articles;
}
