package com.introlab.techcrunch.web;

import com.introlab.techcrunch.dao.ArticleDaoService;
import com.introlab.techcrunch.entity.dto.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class AppController {
    @Autowired
    ArticleDaoService daoService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "index.html";
    }

    @RequestMapping(value = "/articles/last", method = RequestMethod.GET)
    @ResponseBody
    public List<Article> getTopArticles() {
        return daoService.getArticles();
    }
}
