package com.introlab.techcrunch.repository;

import com.introlab.techcrunch.entity.dto.Article;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ArticleRepository extends JpaRepository<Article, Long> {
    List<Article> findTop10ByOrderByPublishedAtDesc();
    Article findByUrl(String url);
}
