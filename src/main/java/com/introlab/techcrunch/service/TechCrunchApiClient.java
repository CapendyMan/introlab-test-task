package com.introlab.techcrunch.service;

import com.introlab.techcrunch.entity.domain.ArticlesApiResponse;
import com.introlab.techcrunch.entity.dto.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

@Component
public class TechCrunchApiClient {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${techcrunch.api.url}")
    private String techCrunchApiUrl;

    @Value("${techcrunch.api.key}")
    private String techCrunchApiKay;

    @Retryable(maxAttempts = 5, value = RuntimeException.class, backoff = @Backoff(delay = 500, multiplier = 2))
    public List<Article> getArticles() {
        ArticlesApiResponse response = restTemplate.getForObject(String.format(techCrunchApiUrl, techCrunchApiKay), ArticlesApiResponse.class);
        return response.getArticles();
    }

    @Recover
    public List<Article> recoverArticles() {
        return Collections.emptyList();
    }
}
