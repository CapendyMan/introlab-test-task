package com.introlab.techcrunch;


import com.introlab.techcrunch.dao.ArticleDaoService;
import com.introlab.techcrunch.entity.dto.Article;
import com.introlab.techcrunch.service.TechCrunchApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TechCrunchScheduledApp {
    @Autowired
    ArticleDaoService articleDaoService;
    @Autowired
    TechCrunchApiClient apiClient;
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Scheduled(fixedRate = 60000)
    public void getArticlesFromApi() {
        List<Article> newArticles = articleDaoService.saveArticles(apiClient.getArticles());
        if (!newArticles.isEmpty()) {
            messagingTemplate.convertAndSend("/articles/new", apiClient.getArticles());
        }
    }
}
