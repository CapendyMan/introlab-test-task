package com.introlab.techcrunch.dao;

import com.introlab.techcrunch.entity.dto.Article;

import java.util.List;

public interface ArticleDaoService {
    List<Article> saveArticles(List<Article> articles);
    List<Article> getArticles();
}
