package com.introlab.techcrunch.dao;

import com.introlab.techcrunch.entity.dto.Article;
import com.introlab.techcrunch.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ArticleDaoServiceImpl implements ArticleDaoService {
    @Autowired
    private ArticleRepository repository;

    @Override
    public List<Article> saveArticles(List<Article> articlesFromApi) {
        return articlesFromApi
                .stream()
                .filter(article -> repository.findByUrl(article.getUrl()) == null)
                .map(article -> repository.save(article))
                .collect(Collectors.toList());
    }

    @Override
    public List<Article> getArticles() {
        return repository.findTop10ByOrderByPublishedAtDesc();
    }
}
